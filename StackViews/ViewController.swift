//
//  ViewController.swift
//  StackViews
//
//  Created by Sergey Bavykin on 4/1/16.
//  Copyright © 2016 Sergey Bavykin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    private static let imageCount = 6
    private var index = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        showImage(0)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func onNextButtonPressed(sender: AnyObject) {
        index = (index + 1) % ViewController.imageCount
        showImage(index)
    }
    
    
    @IBAction func onPreviousButtonPressed(sender: AnyObject) {
        index = index == 0 ? ViewController.imageCount - 1 : --index
        showImage(index)
    }
    
    
    @IBAction func onResetButtonPressed(sender: AnyObject) {
        index = 0
        showImage(0)
    }
    
    private func showImage(index: Int) {
        label.text = "Image \(index)"
        imageView.image = UIImage(named: "image\(index)")
    }
}

